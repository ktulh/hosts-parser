const amqplib = require("amqplib");
const wretch = require("wretch");
global.fetch = require("node-fetch");

const { AMQP_CONNECTION, HUNTER_KEY } = process.env;

const TASKS_QUEUE = "tasks";
const RESULT_QUEUE = "results";

let channel = null;
const testAmqp = async () => {
  var open = amqplib.connect(AMQP_CONNECTION);

  // Publisher
  open
    .then(function (conn) {
      return conn.createChannel();
    })
    .then(function (ch) {
      return ch.assertQueue(RESULT_QUEUE).then(function (ok) {
        channel = ch;
      });
    })
    .catch(console.warn);

  // Consumer
  open
    .then(function (conn) {
      return conn.createChannel();
    })
    .then(function (ch) {
      return ch.assertQueue(TASKS_QUEUE).then(function (ok) {
        return ch.consume(TASKS_QUEUE, async function (msg) {
          if (msg !== null) {
            const domains = JSON.parse(msg.content.toString());
            console.log("domains", domains);
            const results = await Promise.all(
              domains.map(async ({ domain, id }) => {
                const {
                  data: { emails = [] },
                } = await wretch("https://api.hunter.io/v2/domain-search")
                  .query({
                    domain,
                    api_key: HUNTER_KEY,
                  })
                  .get()
                  .json();
                const results = emails.map(
                  ({ first_name, last_name, value: email, confidence }) => ({
                    domain_id: id,
                    first_name,
                    last_name,
                    email,
                    confidence,
                  })
                );
                return results;
              })
            );

            channel.sendToQueue(
              RESULT_QUEUE,
              Buffer.from(JSON.stringify(results.flat()))
            );
            ch.ack(msg);
          }
        });
      });
    })
    .catch(console.warn);
};

testAmqp();
